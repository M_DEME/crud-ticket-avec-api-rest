<?php
    
   require_once ('model/TicketStorageMysql.php');
    require_once ('controller/Controller.php');
      class Router{
      	public function main($bd){
      		$ticketStorageMysql = new ticketStorageMysql($bd);
             $controller = new Controller($ticketStorageMysql);
             $tab = array();
             $tab['success'] = "false";
             $tab['result'] = null;
             $action = key_exists('action', $_GET)? $_GET['action']: null;
             // switch suivant les parametres passée en GET
             switch ($action) {
              // faire appel au controller pour retourner toute les ticket disponible
               case "list":
                  $tab['success'] = true;
                  $tab['result'] = $controller->showAllTicket();
               break;
               // lecture d'un seul ticket 
               case "read":
                 if(key_exists('id', $_GET))
                   {
                     $tab['success'] = true;
                     $tab['result'] = $controller->showTicket($_GET['id']);
                   }
                 else
                  {
                  	$tab['success'] = false;
                     $tab['result'] = "identifiant non disponible";
                  }	
               break;
               // enregistrer des donnee recue en POST dans la base de donnee  
               case "enregistrer":                    
                    $tab['success'] = true;
                    $tab['result'] = $controller->insertTicket($_POST);
               break;
               case "modifier":                 
                  $tab['success'] = true;
                  $tab['result'] = $controller->showUpdateTicket($_POST);           
               break;
               case "supprimmer":
               if($controller->deleteTicketController($_POST['id'])==true){
                    $tab['success'] = true;
                    $tab['result'] = "suppression fait avec succes";
                }else{
                	$tab['success'] = false;
                    $tab['result'] = "suppression impossible";
                }    
               break;
         	 default:
                  $tab['success'] = false;
                  $tab['result'] = null;
             break;
          }
          // methode appelle pour retourner les donnes au format JSON suivant ce qui est donnee en argument
             self::contentJson($tab["success"],$tab['result']);
      	}         
         // methode pour encoder les donnees au format json
         public function contentJson($success,$result=NULL)
         {
               $tab['success'] = $success;
               $tab['result'] = $result;
         	   if($result==null){
                  $tab['success'] = false;
         	   }
             echo json_encode($tab);
         }
      }
?>