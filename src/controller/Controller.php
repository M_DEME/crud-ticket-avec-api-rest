<?php
   require_once ('model/TicketStorageMysql.php');
   class Controller
   {
       private $ticketStorageMysql;
       function __construct(TicketStorageMysql $ticketMysql)
       {
         $this->ticketStorageMysql = $ticketMysql;
        }      

      public function showAllTicket()
      {
         return $this->ticketStorageMysql->readAll();
      }
      public function showTicket($id)
      {      	
         return $this->ticketStorageMysql->read($id);
      }
      // controller de la creation d'un ticket avec les tous champs requis
      public function insertTicket($data)
      {
      	if($data['date']!=null && $data['texte']!=null && $data['severite']!=null && ($data['severite']==="normal" || $data['severite']==="bas" || $data['severite']==="urgent")){
      	  return $this->ticketStorageMysql->createTicket($data);
      	}else
      	{
      		return null;
      	}
      }
      //controller d'une modificationk d'un ticket 
      public function showUpdateTicket($data)
      {
      	 if($data['date']!=null && $data['texte']!=null && $data['severite']!=null && ($data['severite']==="normal" || $data['severite']==="bas" || $data['severite']==="urgent")){
      	   return $this->ticketStorageMysql->changeTicket($data);
      	}else
      	{
      		return null;
      	}
      }
      public function deleteTicketController($id)
      {
      	  return $this->ticketStorageMysql->deleteTicket($id);
      }
   }
?>