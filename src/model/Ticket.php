<?php
   
   class Ticket
   {
      private  $id;
      private  $date;
      private  $texte;
      private  $severite;

      public function __construct($id,$date,$texte,$severite)
      {
         $this->id = $id;
         $this->date = $date;
         $this->texte = $texte;
         $this->severite = $severite;
      }
      public function setDate($date)
      {
      	 $this->date = $date;
      }
      public function setTexte($texte)
      {
      	 $this->texte = $texte;
      }
      public function setSeverite($severite)
      {
      	 $this->severite = $severite;
      }
      public function getDate()
      {
      	  return $this->date; 
      }
      public function getTexte()
      {
      	  return $this->texte; 
      }
      public function getSeverite()
      {
      	  return $this->severite; 
      }
   }
?>