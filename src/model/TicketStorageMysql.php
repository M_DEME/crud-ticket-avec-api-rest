<?php
   require_once ('model/TicketStorageMysql.php');
   // class destiné pour les requetes sql 
   class TicketStorageMysql
   {
       private $bd;
       public function __construct($bd)
       {
          $this->bd = $bd;
       }
       // lecture de toute les ticket
       public function readAll()
       {
       	   $req =  $this->bd->prepare('SELECT * FROM ticket');
           $req->execute();
           $result = $req->fetchAll();
           $list = array();
           foreach ($result as $key => $value) {
           	   $list[$value['id']] = array("id"=>$value['id'],"date"=>$value['date'],"texte"=>$value['texte'],"severite"=>$value['severite']);
           }
           return $list;
       }
       //lecture d'un ticket donnee en parametre
       public function read($id)
       {
       	   $req =  $this->bd->prepare('SELECT * FROM ticket WHERE id=:identifiant');
       	   $req->bindParam(':identifiant', $id);
       	   $req->execute();
           $result = $req->fetch();
           if($result['id']){
               $data = array("id"=>$result['id'],"date"=>$result['date'],"texte"=>$result['texte'],"severite"=>$result['severite']);
                return $data;
           }
           return null;
       }
       // cree un ticket
       public function createTicket($data){
        $req = $this->bd->prepare('INSERT INTO ticket (date,texte,severite) values (:date,:texte,:severite)');
        $req->bindParam(':date',$data['date']);	
        $req->bindParam(':texte',$data['texte']);
        $req->bindParam(':severite',$data['severite']);
        $req->execute();
        $id = $this->bd->lastInsertId(); 
        $result = array("id"=>$id,"date"=>$data['date'],"texte"=>$data['texte'],"severite"=>$data['severite']);
        return $result;
     }

     //modifier un ticket
     public function changeTicket($data)
     {
          if ($this->read($data['id'])!=null) {
            $req= $this->bd->prepare("UPDATE ticket SET date=:date, texte=:texte, severite=:severite WHERE id=:identifiant");
            $req->bindParam(':date',$data['date']);	
            $req->bindParam(':texte',$data['texte']);
            $req->bindParam(':severite',$data['severite']);
            $req->bindParam(':identifiant',$data['id']);
            $req->execute();
            $result = array("id"=>$data['id'],"date"=>$data['date'],"texte"=>$data['texte'],"severite"=>$data['severite']);         
            return $data;
          }
          return null;
     }
     //suppression d'un ticket
     public function deleteTicket($id)
     {
     	if($this->read($id)!=null){
        $req=$this->bd->prepare('delete   from ticket where id= :identifiant');
        $req->bindParam(':identifiant',$id);
        $req->execute();
        return true;
        }
        return false;     
     }
   }
?>