<?php
header('Content-type: application/json');


set_include_path("./src");

require_once("Router.php");
// connexion a base de donnee
try
{
    $dsn = 'mysql:host=localhost;port=3306;dbname=api;charset=utf8';
    $user = 'root';
    $pass = '';
    $bd = new PDO($dsn, $user, $pass);
    
}
catch(erreur $e)
{
    die('error'.$e->getMessage());
}


$router = new Router();
$router->main($bd);

?>